package org.academiadecodigo.stackinvaders.guessthenumber;

public class Player {

    private String name;

    public Player(String name) {
        this.name = name;
    }

    int chooseNumber(int max) {
        return Randomizer.generateNumber(10);

    }

    String getName() {
        return name;
    }

}
