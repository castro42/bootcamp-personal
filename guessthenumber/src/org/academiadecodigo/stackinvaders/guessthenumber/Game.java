package org.academiadecodigo.stackinvaders.guessthenumber;

import java.util.ArrayList;

public class Game {

    private int numberToGuess;
    private boolean gameOver = false;



    void start(Player[] playersArray) {

        this.numberToGuess = Randomizer.generateNumber(10);
        System.out.println("The correct number is " + numberToGuess);

        while(!gameOver) {

            for (Player currentPlayer : playersArray) {
                System.out.println(currentPlayer.getName() + " is thinking...");
                int playerGuess = currentPlayer.chooseNumber(10);
                System.out.println(currentPlayer.getName() + " chose " + playerGuess);

                if (playerGuess == numberToGuess) {
                    System.out.println(currentPlayer.getName() + " has guessed correctly!");
                    System.out.println("The number was " + playerGuess);
                    System.out.println(currentPlayer.getName() + " you are AMAZING!!!!");
                    gameOver = true;
                    break;
                }
            }



        }

    }



    public int getNumberToGuess() {
        return numberToGuess;
    }

    public boolean getGameOver() {
        return this.gameOver;
    }

    public void setGameOver(boolean trueOrFalse) {
        this.gameOver = trueOrFalse;
    }


}
