package org.academiadecodigo.stackinvaders.guessthenumber;

public class Randomizer {

    static int generateNumber(int max) {
        int number = (int)(Math.random() * max);
        return number;
    }
}
