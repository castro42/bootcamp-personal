package src.org.academiadecodigo.tailormoons;

import java.io.FileNotFoundException;

public class Main {

    public static void main(String[] args) {


        WordReader wordReader = new WordReader();
        try {
            wordReader.readFileByWord("/Users/codecadet/Desktop/argon.txt");
        } catch (FileNotFoundException e) {
            System.out.println("FILE NOT FOUND!");
        }

        for (String word : wordReader)  {
            System.out.println(word);
        }

     /*   The default implementation behaves as if:

        while (hasNext())
            action.accept(next());*/


        }
}
