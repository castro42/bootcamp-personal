package src.org.academiadecodigo.tailormoons;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Iterator;

public class WordReader implements Iterable<String> {

    FileReader fileReader;
    BufferedReader bufferedReader;

    public void readFileByWord(String file) throws FileNotFoundException {
        fileReader = new FileReader(file);
        bufferedReader = new BufferedReader(fileReader);

    }


    @Override
    public Iterator<String> iterator() {
        return new WordIterator();
    }


    private class WordIterator implements Iterator<String> {

        private String currentLine;
        private int index = -1;
        private String[] currentLineArray;
        
        WordIterator() {
            try {
                currentLine = bufferedReader.readLine();
                currentLineArray = currentLine.split(" ");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        @Override
        public boolean hasNext() {

            if (index + 1 < currentLineArray.length) {
                return true;
            }

            try {
                currentLine = bufferedReader.readLine();

                if (currentLine == null) {
                    return false;
                }

                index = -1;
                currentLineArray = currentLine.split(" ");
                return hasNext();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return false;

        }

        @Override
        public String next() {


            index++;
            System.out.println("Index: " + index);
            System.out.println(currentLineArray.length);
            return currentLineArray[index];

        }

    }
}

