package src.org.academiadecodigo.tailormoons.chatserver.server;

import java.io.IOException;

public class Main {

    public static void main(String[] args) {

        try {
            Server myAmazingChatServer = new Server(5000);
            myAmazingChatServer.listen();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
}
