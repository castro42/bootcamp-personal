package src.org.academiadecodigo.tailormoons.chatserver.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Server {

    private ServerSocket bindSocket = null;
    public static final int MAX_CONCURRENT_CLIENTS = 5;
    private List usersList;


    public Server (int port) throws IOException {
        bindSocket = new ServerSocket(port);
        usersList = new LinkedList();
    }

    public void listen() {

        try {

                ExecutorService service = Executors.newFixedThreadPool(MAX_CONCURRENT_CLIENTS);

                while(true) {

                    //accepts client connections and instantiates the client dispatcher
                    ClientHandler clientHandler = new ClientHandler(bindSocket.accept(), this, "User" + usersList.size() +1);
                    usersList.add(clientHandler);


                    //Launch the client thread
                    service.submit(clientHandler);
                }

            } catch (IOException e) {
                System.out.println(e.getMessage());
            }


    }


}
