package src.org.academiadecodigo.tailormoons.chatserver.server;

public class User {

    private String nickname;

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }
}
