package src.org.academiadecodigo.tailormoons.chatserver.server;

import java.io.*;
import java.net.Socket;

public class ClientHandler implements Runnable{

    private Socket clientSocket;
    private Server chatServer;
    private BufferedReader in;
    private PrintWriter out;
    private String username;

    //input stream
    //output stream
    //adicionar instancia à linked list


    public ClientHandler(Socket clientSocket, Server chatServer, String username) {
        this.clientSocket = clientSocket;
        this.chatServer = chatServer;
        this.username = username;

        // client -> client handler -> chat server
        try {
            in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
        } catch (IOException e) {
            e.printStackTrace();
        }

        // chat server -> client handler -> client
        // FAZER
        try {
            out = new PrintWriter(clientSocket.getOutputStream(), true);
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    @Override
    public void run() {

            while(true) {
                try {
                    String newLine = in.readLine();
                    out.println(username + "says : " + newLine);

                } catch (IOException e) {
                    System.out.println(e.getMessage());
                }
            }


    }


}
