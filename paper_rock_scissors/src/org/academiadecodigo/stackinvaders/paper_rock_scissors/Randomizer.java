package org.academiadecodigo.stackinvaders.paper_rock_scissors;

public class Randomizer {

    static HandType randomizeHand() {
        int number = (int)(Math.random() * 3); //refactor: calculate number of hand-types instead of hard-code

        switch (number) {
            case 0:
                return HandType.PAPER;
            case 1:
                return HandType.ROCK;
            case 2:
                return HandType.SCISSORS;
            default:
                return null;
        }

    }
}
