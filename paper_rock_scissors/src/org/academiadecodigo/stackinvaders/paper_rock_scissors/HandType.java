package org.academiadecodigo.stackinvaders.paper_rock_scissors;

public enum HandType {
    PAPER(0),
    ROCK(1),
    SCISSORS(2);

    public final int handValue;

    HandType(int handValue) {
        this.handValue = handValue;
    }


}
