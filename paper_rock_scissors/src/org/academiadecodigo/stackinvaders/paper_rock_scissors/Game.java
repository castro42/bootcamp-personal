package org.academiadecodigo.stackinvaders.paper_rock_scissors;

public class Game {

    private int maxVictories = 5;

    void start() {

        //refactor: move these constructors to Main
        Player playerOne = new Player("Pedro");
        Player playerTwo = new Player("Pandora");

        //play a round (note: first to win 5 rounds wins the game)
        while((playerOne.getVictories() <= 5) && (playerTwo.getVictories() <= 5)) {
            HandType playerOneHand = playerOne.selectHand();
            HandType playerTwoHand = playerTwo.selectHand();
            System.out.println(playerOne.getName() + " has chosen " + playerOneHand);
            System.out.println(playerTwo.getName() + " has chosen " + playerTwoHand);


            if ((playerOneHand == HandType.SCISSORS) && (playerTwoHand == HandType.PAPER)) {
                System.out.println(playerOne.getName() + " has won this round");
                playerOne.incrementVictories();

            } else if ((playerOneHand == HandType.SCISSORS) && (playerTwoHand == HandType.ROCK)) {
                System.out.println(playerTwo.getName() + " has won this round");
                playerTwo.incrementVictories();

            } else if ((playerOneHand == HandType.PAPER) && (playerTwoHand == HandType.SCISSORS)) {
                System.out.println(playerTwo.getName() + " has won this round");
                playerTwo.incrementVictories();

            } else if ((playerOneHand == HandType.PAPER) && (playerTwoHand == HandType.ROCK)) {
                System.out.println(playerOne.getName() + " has won this round");
                playerOne.incrementVictories();

            } else if ((playerOneHand == HandType.ROCK) && (playerTwoHand == HandType.SCISSORS)) {
                System.out.println(playerOne.getName() + " has won this round");
                playerOne.incrementVictories();

            } else if ((playerOneHand == HandType.ROCK) && (playerTwoHand == HandType.PAPER)) {
                System.out.println(playerTwo.getName() + " has won this round");
                playerTwo.incrementVictories();

            } else {
                System.out.println("This round is a draw!");
            }

            System.out.println(playerOne.getVictories());
            System.out.println(playerTwo.getVictories());
        }

        Player.win(playerOne, playerOne);
    }

    private int getMaxVictories() {
        return maxVictories;
    }

    public void incrementMaxVictories() {
        maxVictories++;
    }
}
