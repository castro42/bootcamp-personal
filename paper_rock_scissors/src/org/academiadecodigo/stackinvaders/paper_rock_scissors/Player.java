package org.academiadecodigo.stackinvaders.paper_rock_scissors;

public class Player {

    private String name;
    private int victories;

    public Player(String name) {
        this.name = name;
    }

    public HandType selectHand() {
        HandType hand = Randomizer.randomizeHand();
        return hand;
    }

    public static void win(Player playerOne, Player playerTwo) {
        if (playerOne.getVictories() > playerTwo.getVictories()) {
            System.out.println(playerOne.getName() + " has won!!!");
        } else {
            System.out.println(playerTwo.getName() + " has won!!!");
        }
    }

    public String getName() {
        return this.name;
    }

    public int getVictories() {
        return this.victories;
    }

    public void incrementVictories() {
        this.victories++;
    }
}
