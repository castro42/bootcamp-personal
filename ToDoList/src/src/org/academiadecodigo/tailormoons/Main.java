package src.org.academiadecodigo.tailormoons;

public class Main {

    public static void main(String[] args) {

        ToDoList<Task> aToDoList = new ToDoList();

        Task task1 = new Task("comprar pepinos", TaskImportance.HIGH, 2);
        Task task2 = new Task("pagar luz", TaskImportance.LOW, 1);
        Task task3 = new Task("ver o BM", TaskImportance.MEDIUM, 3);
        Task task4 = new Task("comprar jornal", TaskImportance.HIGH, 1);


        System.out.println(aToDoList.add(task1));
        System.out.println(aToDoList.add(task2));
        System.out.println(aToDoList.add(task3));
        System.out.println(aToDoList.add(task4));

        System.out.println(aToDoList.toString());


       /* while (!aToDoList.isEmpty()) {
            //System.out.println(aToDoList.);
            System.out.println(aToDoList.remove());
        }*/

        System.out.println(aToDoList.peek());
    }
}
