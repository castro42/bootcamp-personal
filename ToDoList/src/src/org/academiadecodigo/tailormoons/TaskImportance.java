package src.org.academiadecodigo.tailormoons;

public enum TaskImportance {
    HIGH(1),
    MEDIUM(2),
    LOW(3);

    private int value;

    TaskImportance(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
