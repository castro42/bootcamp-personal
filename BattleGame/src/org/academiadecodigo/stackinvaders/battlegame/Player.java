package org.academiadecodigo.stackinvaders.battlegame;

public class Player {

    private String name;
    private Fighter[] fighters;

    public Player(String name, Fighter[] fighters) {
        this.name = name;
        this.fighters = fighters;
    }

    public void attack(Player player) {

        //Selects random fighter from array

        int length = fighters.length;
        Fighter selectedFighter;
        Fighter selectedOpponentFighter;

        while(true) {
            int index = (int) (Math.random() * length);
            if(!fighters[index].isDead()) {
                selectedFighter = fighters[index];
                break;
            }
        }
        System.out.println("Player1 selected " + selectedFighter);

        //Selects random fighter from opponent's array
        //TODO: PROBLEM: INDEX 0 NEVER GETS PICKED
        Fighter[] array = player.getFighters();
        int arrayLength = array.length;

        while(true) {
            int index = (int) (Math.random() * arrayLength);
            if(!array[index].isDead()) {
                selectedOpponentFighter = array[index];
                break;
            }
        }
        System.out.println("Player2 selected " + selectedOpponentFighter);


        //Hit (0) or Cast(1)
        if (Randomizer.getRandomNumber(1) == 0) {
            System.out.println(selectedFighter.getName() + " punches " + selectedOpponentFighter.getName());
            selectedFighter.hit(selectedOpponentFighter);
        } else { // == 1
            System.out.println(selectedFighter.getName() + " throws a fireball to " + selectedOpponentFighter.getName());
            selectedFighter.cast(selectedOpponentFighter);
        }

    }

    public boolean lost() {
        System.out.println(getName() + ": You lost the game!!");
        return true;
    }

    public String getName() {
        return name;
    }

    public Fighter[] getFighters() {
        return fighters;
    }
}
