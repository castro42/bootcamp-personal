package org.academiadecodigo.stackinvaders.battlegame;

public class Main {

    public static void main(String[] args) {

        //Randomizer randomizer = new Randomizer();

        //Create fighters and Player 1
        Wizard wizard1 = new Wizard("Abramelin1", 7, 3, 6);

        Fighter fighter1 = new Gladiator("Hulk1", 9, 6, 2);

        Troll troll1 = new Troll("Ragnarok1", 9, 8, 1);

        Player player1 = new Player("Pedro1", new Fighter[]{wizard1, fighter1, troll1});

        //Create fighters and Player 2
        Wizard wizard2 = new Wizard("Crowley2", 7, 1, 8);

        Fighter fighter2 = new Gladiator("Undertaker2", 8, 7, 1);

        Troll troll2 = new Troll("Ktulu2", 9, 5, 1);

        Player player2 = new Player("Catarina2", new Fighter[]{wizard2, fighter2, troll2});

        Arena arena1 = new Arena(player1, player2);
        arena1.battle();



        /*Let's create a battle simulation game!
        Our game will have Fighters. Each fighter can hit or cast a spell on another fighter (cause damage). The amount of damage caused will depend on how much attack damage (for hits) or spell damage (for casts) the attacking fighter has.
        There are 3 specific types of fighters (Mages, Trolls and Gladiators), which attack and suffer damage in a different way:
        - When a mage casts a spell, he has a chance to create a shield for himself - which means the next time he gets attacked no damaged will be dealt.
        - When a gladiator hits there is a chance that double the damage is caused (critical).
                - Some of the time a troll is ordered to attack, it might not do anything (trolls, right??)
        Our game is going to be played by two players. Each player will have an array with his fighters (a mix of the three types).
                Players will take turns attacking each other. For a player to attack he should first choose a random alive Fighter from his array, and select a random alive Fighter from the opponent's array. After both Fighters have been chosen, the attacking attacking player will randomly tell his fighter to hit or cast a spell on the opponent's fighter.
        The game finishes when a player has lost all fighters (all dead).*/
    }
}
