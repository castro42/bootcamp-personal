package org.academiadecodigo.stackinvaders.battlegame;


/**
 * Class with some common Fighter behaviour
 * NOT MEANT to be instantiated
 */

public class Fighter {
    private String name;
    private int attackDamage;
    private int spellDamage;
    private int health;

    public Fighter(String name, int health, int attackDamage, int spellDamage) {
        this.name = name;
        this.health = health;
        this.attackDamage = attackDamage;
        this.spellDamage = spellDamage;
    }

    public void hit(Fighter fighter) {

        System.out.println("this is the Fighter hit");
        fighter.suffer(attackDamage);
    }

    public void cast(Fighter fighter) {
        fighter.suffer(spellDamage);
    }

    public void suffer(int damage) {
        System.out.println(getName() + " has " + health + " health.");
        System.out.println(getName() + " gets " + damage + " damage.");
        health = health - damage;
        System.out.println(getName() + " has " + health + " health.");
    }

    public boolean isDead() {
        return health <= 0;
    }

    public String getName() {
        return name;
    }

    public int getHealth() {
        return health;
    }
}
