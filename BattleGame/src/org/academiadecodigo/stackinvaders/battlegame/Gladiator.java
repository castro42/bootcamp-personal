package org.academiadecodigo.stackinvaders.battlegame;

public class Gladiator extends Fighter {

    public Gladiator(String name, int health, int attackDamage, int spellDamage) {
        super(name, health, attackDamage, spellDamage);
    }

    public void hit(Fighter fighter) {
        System.out.println("This is the gladiator hit");

        //TODO: Deliver critical hit all at once instead of in 2 blows
        if(Randomizer.getRandomNumber(1) == 0) {
            System.out.println("Gladiator will deal a critical hit! Double damage this turn!");
            super.hit(fighter);
        }

        super.hit(fighter);
    }
}
