package org.academiadecodigo.stackinvaders.battlegame;

public class Arena {

    private Player playerOne;
    private Player playerTwo;

    public Arena(Player playerOne, Player playerTwo) {
        this.playerOne = playerOne;
        this.playerTwo = playerTwo;
    }

    public void battle() {

        int round = 1;
        boolean play = true;
        while(play) {

            System.out.println("play " + play);

            System.out.println("______________________________");
            System.out.println("Round " + round);
            System.out.println("______________________________");

            for (Fighter fighter : playerOne.getFighters()) {
                if(fighter.getHealth() <= 0) {
                    play = false;
                    //System.out.println("Player one lost");
                    playerOne.lost();
                    break;
                }



                System.out.println("break1");
                System.out.println(fighter.getName() + " " + fighter.getHealth());

            }

            for (Fighter fighter : playerTwo.getFighters()) {
                if(fighter.getHealth() <= 0) {
                    play = false;
                    //System.out.println("Player two lost");
                    playerTwo.lost();
                    break;
                }

                System.out.println("break2");
                System.out.println(fighter.getName() + " " + fighter.getHealth());


            }

            playerOne.attack(playerTwo);
            playerTwo.attack(playerOne);
            round = round + 1;
        }


    }
}
