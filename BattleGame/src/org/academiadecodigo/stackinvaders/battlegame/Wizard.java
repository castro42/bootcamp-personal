package org.academiadecodigo.stackinvaders.battlegame;

public class Wizard extends Fighter {

    private boolean hasShield;

    public Wizard(String name, int health, int attackDamage, int spellDamage) {
        super(name, health, attackDamage, spellDamage);
    }

    public void cast(Fighter fighter) {

        if(Randomizer.getRandomNumber(1) == 0) {
            activateShield();
            System.out.println("The wizard activated a shield!");
        }

        super.cast(fighter);

    }

    public void suffer(int damage) {
        System.out.println("hasShield: " + hasShield);
        if(hasShield) {
            System.out.println(getName() + " has a shield! No damage taken!");
            deactivateShield();
            return;
        }
        super.suffer(damage);
    }

    public boolean getHasShield() {
        return hasShield;
    }

    public void activateShield() {
        hasShield = true;
    }

    public void deactivateShield() {
        hasShield = false;
    }


}
