package org.academiadecodigo.stackinvaders.battlegame;

public class Troll extends Fighter {

    public Troll(String name, int health, int attackDamage, int spellDamage) {
        super(name, health, attackDamage, spellDamage);
    }

    public void hit(Fighter fighter) {
        System.out.println("this is the Troll hit");
        if(Randomizer.getRandomNumber(1) == 0) {
            System.out.println("I just ate a magnificent feijoada... Don't feel like fighting.");
            return;
        }

        super.hit(fighter);



    }
}
