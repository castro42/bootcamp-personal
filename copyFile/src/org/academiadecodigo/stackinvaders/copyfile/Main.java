package org.academiadecodigo.stackinvaders.copyfile;

import java.io.IOException;
import java.sql.SQLOutput;

public class Main {

    public static void main(String[] args) {

        try {
            FileCopier aFileCopier = new FileCopier();

            aFileCopier.copyFile("sales.txt", "sales2.txt");
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }


    }
}
