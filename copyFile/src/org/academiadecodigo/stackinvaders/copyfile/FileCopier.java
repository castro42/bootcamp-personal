package org.academiadecodigo.stackinvaders.copyfile;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class FileCopier {


    public void copyFile(String source, String destination) throws IOException {


        //Read file
        FileInputStream inputStream = new FileInputStream(source);
        FileOutputStream outputStream = new FileOutputStream(destination);

        System.out.println(inputStream.available());
        int length = 0;
        byte[] buffer = new byte[1024];

        while(length != -1) {

            length = inputStream.read(buffer);
            System.out.println("I have read this many bytes: " + length);

            if (length > 0) {
                outputStream.write(buffer, 0, length);
            }
        }


        System.out.println("Reached the end of file. I have read this many bites: " + length);
        inputStream.close();
        outputStream.close();


    }

}
