package org.academiadecodigo.bootcamp.gfx.simplegfx;

import org.academiadecodigo.bootcamp.gfx.lanterna.LanternaGrid;
import org.academiadecodigo.bootcamp.grid.GridColor;
import org.academiadecodigo.bootcamp.grid.GridDirection;
import org.academiadecodigo.bootcamp.grid.position.AbstractGridPosition;
import org.academiadecodigo.bootcamp.grid.position.GridPosition;
import org.academiadecodigo.simplegraphics.graphics.Color;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;

/**
 * Simple graphics position
 */
public class SimpleGfxGridPosition extends AbstractGridPosition {

    private Rectangle rectangle;
    private SimpleGfxGrid simpleGfxGrid;

    /**
     * Simple graphics position constructor
     * @param grid Simple graphics grid
     */
    public SimpleGfxGridPosition(SimpleGfxGrid grid){
        super((int) (Math.random() * grid.getCols()), (int) (Math.random() * grid.getRows()), grid);
        //simpleGfxGrid = grid;

        //throw new UnsupportedOperationException();
    }

    /**
     * Simple graphics position constructor
     * @param col position column
     * @param row position row
     * @param grid Simple graphics grid
     */
    public SimpleGfxGridPosition(int col, int row, SimpleGfxGrid grid){
        super(col, row, grid);
        int padding = SimpleGfxGrid.PADDING;
        int col2 = col * grid.getCellSize() + padding;
        int row2 = row * grid.getCellSize() + padding;
        rectangle = new Rectangle(col2, row2, grid.getCellSize(), grid.getCellSize());
        rectangle.fill();
    }

    /**
     * @see GridPosition#show()
     */
    @Override
    public void show() {
        rectangle.draw();
        rectangle.fill();
    }

    /**
     * @see GridPosition#hide()
     */
    @Override
    public void hide() {
        rectangle.delete();
    }

    /**
     * @see GridPosition#moveInDirection(GridDirection, int)
     */
    @Override
    public void moveInDirection(GridDirection direction, int distance) {
        int currentX = getCol();
        int currentY = getRow();

        super.moveInDirection(direction, distance);

        int newX = getCol();
        int newY = getRow();

        int cellSize = simpleGfxGrid.getCellSize();


        rectangle.translate((newX-currentX)*cellSize, (newY-currentY)*cellSize);

        //rectangle.translate();
        /*rectangle.delete();
        rectangle = new Rectangle()   */
    }

    /**
     * @see AbstractGridPosition#setColor(GridColor)
     */
    @Override
    public void setColor(GridColor color) {
        super.setColor(color);

        rectangle.setColor(SimpleGfxColorMapper.getColor(color));
        show();
    }

}
