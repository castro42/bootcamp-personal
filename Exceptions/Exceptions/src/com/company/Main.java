package com.company;

import com.company.exceptions.FileNotFoundException;
import com.company.exceptions.NotEnoughPermissionsException;
import com.company.exceptions.NotEnoughSpaceException;

import java.nio.file.FileSystemNotFoundException;

public class Main {

    public static void main(String[] args) {

        FileManager fileManager = new FileManager(1);
        System.out.println(fileManager.getLoggedIn());
        fileManager.logIn();




        try {
             fileManager.createFile("teste1");
        } catch (NotEnoughPermissionsException ex) {
            System.out.println("Please log in first!");
        } catch (NotEnoughSpaceException ex) {
            System.out.println("Please delete a file first!");
        }

        try {
            fileManager.createFile("teste2");
        } catch (NotEnoughPermissionsException ex) {
            System.out.println("Please log in first!");
        } catch (NotEnoughSpaceException ex) {
            System.out.println("Please delete a file first!");
        }

        try {
            fileManager.getFile("teste1");
        } catch (FileNotFoundException ex){
            System.out.println("File not found!");
        }

       /* try {
            fileManager.getFile("stack_invaders");
        } catch (FileNotFoundException ex){
            System.out.println("File not found!");
        }*/

    }
}
