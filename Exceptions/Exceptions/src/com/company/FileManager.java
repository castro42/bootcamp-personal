package com.company;

import com.company.exceptions.FileNotFoundException;
import com.company.exceptions.NotEnoughPermissionsException;
import com.company.exceptions.NotEnoughSpaceException;

public class FileManager {

    private File[] files;
    private Boolean loggedIn;

    public FileManager(int filesLimit) {
        files = new File[filesLimit];
        loggedIn = false;
    }

    public void logIn() {
        setLoggedIn(true);
    }

    public void logOut() {
        setLoggedIn(false);
    }

    public File getFile(String name) throws FileNotFoundException {

        for (int i = 0; i < files.length; i++) {


            if (files[i].getName().equals(name)) {
                System.out.println("Opening file " + files[i].getName() + " ...");
                return files[i];
            }

        }

        /*for (File file : files) {
            System.out.println(file.getName());
            if (file.getName().equals(name)) {
                System.out.println("Opening file " + file.getName() + " ...");
                return file;
            }
        }*/

        throw new FileNotFoundException();
    }


    public void createFile(String name) throws NotEnoughSpaceException, NotEnoughPermissionsException {

        System.out.println("Creating file " + name + " ...");
        if (!(getLoggedIn())) {
            throw new NotEnoughPermissionsException();
        }


        for (int i = 0; i < files.length; i++) {
            if (!(files[i] instanceof File)) {
                files[i] = new File(name);
                System.out.println(i);
                System.out.println("File " + name + " was successfully created!");
                return;
            }

        }

        throw new NotEnoughSpaceException();


    }


    public void setLoggedIn(Boolean loggedIn) {
        this.loggedIn = loggedIn;
    }

    public Boolean getLoggedIn() {
        return loggedIn;
    }

}
