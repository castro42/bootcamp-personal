package org.academiadecodigo.bootcamp.concurrency.bqueue;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;

/**
 * Blocking Queue
 * @param <T> the type of elements stored by this queue
 */
public class BQueue<T> {

    private int limit;
    private boolean isFull;
    private boolean isEmpty;
    private Queue<T> queue;

    /**
     * Constructs a new queue with a maximum size
     * @param limit the queue size
     */
    public BQueue(int limit) {

        this.limit = limit;
        queue = new LinkedList<T>();

    }

    /**
     * Inserts the specified element into the queue
     * Blocking operation if the queue is full
     * @param data the data to add to the queue
     */
    public void offer(T data) {

        System.out.println(Thread.currentThread().getName() + " trying to add an element to the queue...");

        synchronized (this) {

            while(getIsFull()) {

                try {
                    System.out.println("The queue is full...");
                    System.out.println(Thread.currentThread().getName() + " is waiting...");
                    wait();
                } catch (InterruptedException e) {
                    System.out.println(e.getMessage());
                }
            }

            queue.offer(data);
            System.out.println(Thread.currentThread().getName() + " FINISHED THE INSERTION");

            if (queue.size() == limit) {
                setIsFull(true);
            }


            System.out.println(Arrays.toString(queue.toArray()));

            notifyAll();
        }

    }

    /**
     * Retrieves and removes data from the head of the queue
     * Blocking operation if the queue is empty
     * @return the data from the head of the queue
     */
    public T poll() {

        System.out.println(Thread.currentThread().getName() + " trying to remove an element to the queue...");

        synchronized (this) {

            while(queue.size() == 0) {

                try {
                    System.out.println("The queue is empty, no elements to remove...");
                    System.out.println(Thread.currentThread().getName() + " is waiting...");
                    wait();
                } catch (InterruptedException e) {
                    System.out.println(e.getMessage());
                }
            }

            T removedElement = queue.poll();
            System.out.println(Thread.currentThread().getName() + " FINISHED REMOVING");


            System.out.println(Arrays.toString(queue.toArray()));

            notifyAll();
            return removedElement;
        }

    }

    private boolean setIsEmpty(boolean b) {
        return isEmpty = b;
    }

    /**
     * Gets the number of elements in the queue
     * @return the number of elements
     */
    public int getSize() {

        return queue.size();

    }

    /**
     * Gets the maximum number of elements that can be present in the queue
     * @return the maximum number of elements
     */
    public int getLimit() {

        return limit;

    }

    public boolean getIsFull() {
        return isFull;
    }

    public boolean getIsEmpty() {
        return isEmpty;
    }

    public void setIsFull(boolean full) {
        isFull = full;
    }

}
