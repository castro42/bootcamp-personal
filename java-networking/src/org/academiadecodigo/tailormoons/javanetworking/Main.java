package org.academiadecodigo.tailormoons.javanetworking;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;

public class Main {

    public static void main(String[] args) {

        InetAddress IP = null;
        try {
            IP = InetAddress.getByName("www.terranova.pt");
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }

        System.out.println(IP);

        System.out.println("Host Name: "+IP.getHostName());
        System.out.println("IP Address: "+IP.getHostAddress());

        try {
            System.out.println("IP Address is reachable?: "+IP.isReachable(100));
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
