package org.academiadecodigo.stackinvaders.magiclamp;

public class MagicLamp {

    private int maxGenies;
    private int geniesCreated = 0;
    private int remainingGenies;

    public MagicLamp(int maxGenies) {
        this.maxGenies = maxGenies;
        remainingGenies = maxGenies;
    }

    public Genie rub() {
        int maxWishes = randomizer();

        if (geniesCreated >= maxGenies) {
            /*System.out.println(geniesCreated);
            System.out.println(maxGenies);
            System.out.println(geniesCreated <= maxGenies);*/
            geniesCreated = geniesCreated - 1;
            System.out.println("This magic lamp has no more genies left, bro!");
            return new RecyclableDemon();
        }

        geniesCreated = geniesCreated + 1;
        remainingGenies = remainingGenies - 1;
        System.out.println("Remaining genies: " + remainingGenies);

        if((geniesCreated % 2) == 0) {
            return new FriendlyGenie(maxWishes);
        } else {
            return new GrumpyGenie(maxWishes);
        }
    }

    public void recharge(RecyclableDemon demon) {
        if(demon.recycle() == -1) {
            System.out.println("You can only recycle a demon once!");
        }

        resetGeniesCreated();
        resetRemainingGenies();
        System.out.println("Genies created " + getGeniesCreated());
    }

    public void compare (MagicLamp LampB) {
        System.out.println(" ");
        System.out.println(this);
        System.out.println("Max genies" + this.getMaxGenies());
        System.out.println("Remaining genies: " + this.getRemainingGenies());
        System.out.println(" ");
        System.out.println(LampB.toString());
        System.out.println("Max genies " + LampB.getMaxGenies());
        System.out.println("Remaining genies: " + LampB.getRemainingGenies());
        System.out.println(" ");
    }

    public void resetGeniesCreated() {
        geniesCreated = 0;
    }

    public void resetRemainingGenies() {
        remainingGenies = maxGenies;
    }

    public int getRemainingGenies() {
        return remainingGenies;
    }

    public int getMaxGenies() {
        return maxGenies;
    }

    public int getGeniesCreated() {
        return geniesCreated;
    }

    public void incrementGeniesCreated() {
        geniesCreated = geniesCreated + 1;
    }

    //TODO: create Randomizer class for this method??
    public int randomizer() {
        return (int) (Math.random() * 5);
    }

}
