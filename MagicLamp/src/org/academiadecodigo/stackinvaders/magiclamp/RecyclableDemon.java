package org.academiadecodigo.stackinvaders.magiclamp;

public class RecyclableDemon extends Genie {

    private boolean hasBeenRecycled = false;

    public RecyclableDemon() {
        super(1);
        System.out.println("demon created");
    }


    public void grantWish() {

        if(hasBeenRecycled()) {
            System.out.println("I've been recycled already! No more wishes for you!");
        }

        incrementWishesGranted();
        System.out.println("Your wish has been granted. Demon at your service!");
        setHasBeenRecycled(true);


    }

    public int recycle() {
        if(hasBeenRecycled) {
            return -1;
        }

        setHasBeenRecycled(true);
        return 1;
    }

    public boolean hasBeenRecycled() {
        return hasBeenRecycled;
    }

    public void setHasBeenRecycled(boolean hasBeenRecycled) {
        this.hasBeenRecycled = hasBeenRecycled;
    }
}
