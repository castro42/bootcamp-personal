package org.academiadecodigo.stackinvaders.magiclamp;

public class Genie {
    private int maxWishes;
    private int wishesGranted;

    public Genie(int maxWishes) {
        this.maxWishes = maxWishes;
    }

    public void incrementWishesGranted() {
        wishesGranted = wishesGranted + 1;
    }


    public int getMaxWishes() {
        return maxWishes;
    }

    public int getWishesGranted() {
        return wishesGranted;
    }

    public void setWishesGranted(int wishesGranted) {
        this.wishesGranted = wishesGranted;
    }

    public void grantWish() {

        System.out.println("Testing grant wish from superclass genie");

        if(getWishesGranted() >= getMaxWishes()) {
            System.out.println("You have no more wishes left!");
            System.out.println("Testing if loop from superclass genie");
            return;
        }

    }
}
