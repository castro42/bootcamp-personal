package org.academiadecodigo.stackinvaders.magiclamp;

public class FriendlyGenie extends Genie {

    public FriendlyGenie(int maxWishes) {
        super(maxWishes);
    }

    @Override
    public void grantWish() {

        /*if(getWishesGranted() >= getMaxWishes()) {
            System.out.println("You have no more wishes left!");
            return
        }*/
        super.grantWish();
        System.out.println("Testing grant wish from friendly genie");
        incrementWishesGranted();
        System.out.println("Your wish has been granted. Use it wisely!");

    }
}
