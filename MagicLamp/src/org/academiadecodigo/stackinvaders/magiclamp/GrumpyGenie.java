package org.academiadecodigo.stackinvaders.magiclamp;

public class GrumpyGenie extends Genie {

    public GrumpyGenie(int maxWishes) {
        super(maxWishes);
    }

    @Override
    public void grantWish() {

        /*if(getWishesGranted() >= getMaxWishes()) {
            System.out.println("You have no more wishes left!");
            return;
        }*/

        super.grantWish();
        System.out.println("Testing grant wish from grumpy genie");
        setWishesGranted(getMaxWishes());
        System.out.println("Your wish has been granted. Use it wisely!");
    }
}
