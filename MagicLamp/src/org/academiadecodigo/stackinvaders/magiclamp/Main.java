package org.academiadecodigo.stackinvaders.magiclamp;

public class Main {

    public static void main(String[] args) {

        MagicLamp lamp1 = new MagicLamp(5);
        MagicLamp lamp2 = new MagicLamp(2);

        //System.out.println(lamp1.getMaxGenies());
        Genie genie1 = lamp1.rub();

        System.out.println(genie1.toString());
        System.out.println("Max wishes " + genie1.getMaxWishes());
        genie1.grantWish();
        System.out.println(genie1.getMaxWishes());
        System.out.println(genie1.getWishesGranted());
        genie1.grantWish();
        genie1.grantWish();


        /*System.out.println(genie1.toString());
        Genie genie2 = lamp1.rub();
        System.out.println(genie2.toString());
        Genie genie3 = lamp1.rub();
        Genie genie4 = lamp1.rub();
        Genie genie5 = lamp1.rub();
        System.out.println("Am i a demon? " + genie4.toString());
        Genie genie6 = lamp1.rub();
        System.out.println("Am i a demon? " + genie6.toString());


        lamp1.recharge((RecyclableDemon) genie6);


        lamp1.compare(lamp2);*/



    }
}
