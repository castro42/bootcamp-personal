package src.org.academiadecodigo.tailormoons;

import org.academiadecodigo.bootcamp.Prompt;
import org.academiadecodigo.bootcamp.scanners.integer.IntegerInputScanner;
import org.academiadecodigo.bootcamp.scanners.integer.IntegerRangeInputScanner;
import org.academiadecodigo.bootcamp.scanners.integer.IntegerSetInputScanner;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

public class Main {

    public static void main(String[] args) {




        Database database = new Database();
        database.populateDatabase();

        // attach prompt to system's input/output
        Prompt prompt = new Prompt(System.in, System.out);
        Login login = new Login(prompt, database);


        login.run();






    }

}
