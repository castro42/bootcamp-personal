package src.org.academiadecodigo.tailormoons;

import java.util.ArrayList;
import java.util.List;

public class Database {

    private List<User> userData;

    public Database () {

        userData = new ArrayList<>();
    }

    public void populateDatabase() {
        userData.add(new User("Pedro", "pamela23"));
        userData.add(new User("Pandora", "divinepoop"));
        userData.add(new User("Almeida", "tracylords"));

    }


    public boolean validateUsername(String username) {

        for (User user : userData) {
            if (user.getUsername().equals(username)) {
                System.out.println("correct username");
                return true;
            }
        }

        System.out.println("No such username!");
        return false;


    }

    public boolean validatePassword(String username, String password) {
        for (User user : userData ) {
            if ((user.getUsername().equals(username)) && (user.getPassword().equals(password))) {
                System.out.println("correct password!");
               return user.getPassword().equals(password);
            }
        }

        System.out.println("Wrong password, try again!");
        return false;
    }



}
