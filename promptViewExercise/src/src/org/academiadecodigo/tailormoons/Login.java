package src.org.academiadecodigo.tailormoons;

import org.academiadecodigo.bootcamp.Prompt;
import org.academiadecodigo.bootcamp.scanners.string.PasswordInputScanner;
import org.academiadecodigo.bootcamp.scanners.string.StringInputScanner;

import java.util.HashMap;

public class Login {

    private Prompt prompt;
    private Database database;


    public Login(Prompt prompt, Database database) {
        this.prompt = prompt;
        this.database = database;

    }

    public void run() {

        System.out.println("Welcome to your first prompt-view experience");
        System.out.println("");
        System.out.println("Insert your login credentials below.");
        System.out.println("");

        StringInputScanner inputUsername = new StringInputScanner();
        inputUsername.setMessage("Login: ");

        String username;
        //get username input and validate it
        while(true) {

            //Block while waiting for input
            username = prompt.getUserInput(inputUsername);

            if (database.validateUsername(username)) {
                break;
            }
        }


        // this scanner hides the input while typing
        PasswordInputScanner inputPassword = new PasswordInputScanner();
        inputPassword.setMessage("Password: ");

        while(true) {

            String password = prompt.getUserInput(inputPassword);

            if (database.validatePassword(username, password)) {
                break;
            }
        }


    }




}


