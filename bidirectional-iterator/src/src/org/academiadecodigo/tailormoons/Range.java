package src.org.academiadecodigo.tailormoons;

import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.function.Consumer;

public class Range implements Iterable<Integer> {

    private int min;
    private int max;
    private boolean forward = true;

    public Range(int min, int max) {
        this.min = min;
        this.max = max;
    }

    public boolean isForward() {
        return forward;
    }

    public void setForward(boolean forward) {
        this.forward = forward;
    }

    @Override
    public Iterator<Integer> iterator() {

        if (forward) {
            return new Iterator() {

                private int current = min - 1;

                @Override
                public boolean hasNext() {
                    return current < max;
                }

                @Override
                public Object next() {
                    if (!hasNext()) {
                        throw new NoSuchElementException();
                    }

                    current++;
                    return current;
                }
            };
        }

            return new Iterator() {

                private int current = max + 1;

                @Override
                public boolean hasNext() {
                    return current > min;
                }

                @Override
                public Object next() {
                    if (!hasNext()) {
                        throw new NoSuchElementException();
                    }

                    current--;
                    return current;
                }
            };

    }
}
