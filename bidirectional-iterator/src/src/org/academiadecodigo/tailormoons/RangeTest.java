package src.org.academiadecodigo.tailormoons;

public class RangeTest {

    public static void main(String[] args) {

        Range myApp = new Range (0, 5);

        for (Integer i : myApp) {
            System.out.println("Iterated: " + i);
        }

        myApp.setForward(false);

        for (Integer i : myApp) {
            System.out.println("Iterated: " + i);
        }
    }
}
