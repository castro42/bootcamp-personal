package org.academiadecodigo.bootcamp.containers;

import java.util.Iterator;

public class LinkedListTest {
    public static void main(String[] args) {

        LinkedList<Integer> numbers = new LinkedList<>();

        numbers.add(21);
        numbers.add(12);
        numbers.add(33);
        numbers.add(10);


        int result = 0;

        for (Integer number : numbers){
            result += number;
        }

        Iterator<Integer> iterator = numbers.iterator();

        while (iterator.hasNext()) {
            Integer number = iterator.next();
            result += number;
        }

        System.out.println(result);
    }

    /**
     * Tests insertion of data objects into the list
     *
     * @return true if all tests passed
     */
    public boolean testAdd() {

        // Preparation
        LinkedList list = new LinkedList();

        // Empty list should contain zero elements
        if (list.size() != 0) {
            return false;
        }

        // List with one element should have size of one
        list.add(new Integer(1));
        if (list.size() != 1) {
            return false;
        }

        // List with two elements should have size of two
        list.add(new Integer(2));
        if (list.size() != 2) {
            return false;
        }

        // List with three elements should have size of three
        list.add(new Integer(3));
        return list.size() == 3;

    }

    /**
     * Tests obtaining the index of data objects from the list
     *
     * @return true if all tests passed
     */
    public boolean testIndex() {

        // Preparation
        LinkedList list = new LinkedList();
        Object data1 = new Integer(1);
        Object data2 = new Integer(2);
        Object data3 = new Integer(3);
        Object data4 = new Integer(4);

        // Tests with empty list
        //System.out.println("Tests with empty list");
        //System.out.println(list.indexOf(data1) != -1);
        if (list.indexOf(data1) != -1) {
            return false;
        }

        // Tests with one element
        list.add(data1);
        //System.out.println("Tests with one element: " );
        //System.out.println(list.indexOf(data1));
        if (list.indexOf(data1) != 0) {
            return false;
        }

        //System.out.println(list.indexOf(data2) != -1);
        if (list.indexOf(data2) != -1) {
            return false;
        }

        // Tests with two elements
        list.add(data2);
        if (list.indexOf(data1) != 0) {
            return false;
        }

        if (list.indexOf(data2) != 1) {
            return false;
        }

        if (list.indexOf(data3) != -1) {
            return false;
        }

        // Tests with three elements
        list.add(data3);
        if (list.indexOf(data1) != 0) {
            return false;
        }

        if (list.indexOf(data2) != 1) {
            return false;
        }

        if (list.indexOf(data3) != 2) {
            return false;
        }

        return list.indexOf(data4) == -1;
    }

    /**
     * Tests getting data objects from the list
     *
     * @return true if all tests passed
     */
    public boolean testGet() {

        // Preparation
        LinkedList list = new LinkedList();
        Object data1 = new Integer(1);
        Object data2 = new Integer(2);
        Object data3 = new Integer(3);

        // Tests with empty list
        if (list.get(0) != null) {
            return false;
        }

        if (list.get(1) != null) {
            return false;
        }

        if (list.get(9) != null) {
            return false;
        }

        // Tests with one element
        list.add(data1);
        //System.out.println((data1.equals(list.get(0))));
        if (!data1.equals(list.get(0))) {
            return false;
        }

        if (list.get(1) != null) {
            return false;
        }

        if (list.get(9) != null) {
            return false;
        }

        // Tests with two elements
        list.add(data2);
        if (!data1.equals(list.get(0))) {
            return false;
        }

        if (!data2.equals(list.get(1))) {
            return false;
        }

        if (list.get(2) != null) {
            return false;
        }

        if (list.get(9) != null) {
            return false;
        }

        // Tests with three elements
        list.add(data3);
        if (!data1.equals(list.get(0))) {
            return false;
        }

        if (!data2.equals(list.get(1))) {
            return false;
        }

        if (!data3.equals(list.get(2))) {
            return false;
        }

        if (list.get(3) != null) {
            return false;
        }

        return list.get(9) == null;

    }

    /**
     * Tests removing data objects from the list
     *
     * @return true if all tests passed
     */
    public boolean testRemove() {

        // Preparation
        LinkedList list = new LinkedList();
        Object data1 = new Integer(1);
        Object data2 = new Integer(2);
        Object data3 = new Integer(3);
        Object data4 = new Integer(4);

        // Test removal with empty list
        //System.out.println(list.remove(data1));
        if (list.remove(data1)) {
            return false;
        }

        // Test removal from list with one element
        list.add(data1);

        //System.out.println((list.remove(data2)));
        if (list.remove(data2)) {
            System.out.println("hhhh");
            return false;
        }

        //System.out.println((!list.remove(data1)));
        if (!list.remove(data1)) {
            System.out.println("zzz");

            return false;
        }

        if (list.size() != 0) {
            System.out.println("dewdewd");
            return false;
        }

        // Test removal from list with two elements
        list.add(data1);
        list.add(data2);

        if (list.remove(data3)) {
            return false;
        }

        if (!list.remove(data1)) {
            return false;
        }

        if (list.size() != 1) {
            return false;
        }

        list.add(data1);
        if (!list.remove(data1)) {
            return false;
        }

        if (!list.remove(data2)) {
            return false;
        }

        if (list.size() != 0) {
            return false;
        }

        // Test removal from list with three elements
        list.add(data1);
        list.add(data2);
        list.add(data3);

        if (list.remove(data4)) {
            return false;
        }

        if (!list.remove(data2)) {
            return false;
        }

        if (list.size() != 2) {
            return false;
        }

        if (list.indexOf(data1) != 0) {
            return false;
        }

        if (list.indexOf(data3) != 1) {
            return false;
        }

        if (!list.remove(data3)) {
            return false;
        }

        return list.size() == 1;
    }

    private void assertCondition(String test, boolean result) {

        System.out.println(test + ": " + (result ? "OK" : "FAIL"));

    }

}
