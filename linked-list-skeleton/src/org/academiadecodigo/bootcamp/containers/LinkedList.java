package org.academiadecodigo.bootcamp.containers;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class LinkedList<T> implements Iterable<T> {

    private Node head;
    private int length = 0;

    public LinkedList() {
        this.head = new Node(null);
    }

    public int size() {
        return length;
    }

    /**
     * Adds an element to the end of the list
     * @param t the element to add
     */
    public void add(T t)  {

        Node node = new Node(t);
        Node iterator = head;
        while (iterator.getNext() != null){
            iterator = iterator.getNext();
        }
        iterator.setNext(node);
        length++;

    }

    /**
     * Obtains an element by index
     * @param index the index of the element
     * @return the element
     */
    public T get(int index) {

        Node currentNode;
        currentNode = head.getNext();
        //System.out.println(currentNode);


        for (int i = 0; i < length; i++) {
            //System.out.println("i==index: ");
            //System.out.println(i == index);
            if (i == index) {
                /*System.out.println(i);
                System.out.println(index);
                System.out.println(currentNode);*/
                return currentNode.getData();
            } else {
                currentNode = currentNode.getNext();
            }
        }

        return null;
    }

    /**
     * Returns the index of the element in the list
     * @param data element to search for
     * @return the index of the element, or -1 if the list does not contain element
     */
    public int indexOf(T data) {

        Node currentNode;
        currentNode = head.getNext();

        int i = 0;

        while( i < length ) {


            //We reached the end of the list (the element wasn't found)
            if (currentNode.getData() == null) {
                return -1;
            }


            if(currentNode.getData().equals(data)) {
                return i;
            }

            currentNode = currentNode.getNext();
            i++;
        }

        //if linked list is empty
        return -1;

    }

    /**
     * Removes an element from the list
     * @param t the element to remove
     * @return true if element was removed
     */

    public boolean remove(T t) {

        LinkedList.Node nodeToTheLeftOfCurrentNode = null;
        LinkedList.Node currentNode = head.getNext();
        int iterator;

        iterator = 0;

        while(!(iterator == length)) {

            //if index to remove is 0
            if(nodeToTheLeftOfCurrentNode == null && currentNode.getData().equals(t)) {
                head.setNext(currentNode.getNext());
                length--;
                return true;
            }

            //if index > 0
            if(currentNode.getData().equals(t)) {
                nodeToTheLeftOfCurrentNode.setNext(currentNode.getNext());
                length--;
                return true;
            }


            nodeToTheLeftOfCurrentNode = currentNode;
            currentNode = currentNode.getNext();
            iterator++;

        }

        return false;


    }

    private class Node {

        private T t;
        private Node next;

        public Node(T t) {
            this.t = t;
            next = null;
        }

        public T getData() {
            return t;
        }

        public void setData(T t) {
            this.t = t;
        }

        public Node getNext() {
            return next;
        }

        public void setNext(Node next) {
            this.next = next;
        }

        @Override
        public Iterator<T> iterator() {
            return new MySpecialIterator(head);

        }
    }

    private class MySpecialIterator implements Iterator<T>{

        public Node currentNode;

        public MySpecialIterator(Node head) {
            currentNode = head;
        }



        public boolean hasNext() {
            System.out.println(currentNode.getNext() != null);
            return currentNode.getNext() != null;
        }

        public T next() {

            currentNode.getNext().getData();
            throw new NoSuchElementException();
        }

    }
}
