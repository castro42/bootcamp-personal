    package com.company;

    import com.company.exceptions.NotEnoughPermissionsException;
    import com.company.exceptions.NotEnoughSpaceException;

    public class FileManager {

    private File[] files;
    private Boolean loggedIn;

    public void FileManager(int filesLimit) {
        files = new File[filesLimit];
        loggedIn = false;
    }

    public void logIn() {
        setLoggedIn(true);
    }

    public void logOut() {
        setLoggedIn(false);
    }

    /*public File getFile(String) {

    }*/

    public void createFile(String name) throws NotEnoughSpaceException, NotEnoughPermissionsException {

        if(!getLoggedIn()) {
                throw new NotEnoughPermissionsException();
        }

            File newFile = new File(name);

            int i;
            for (i = 0; i < files.length; i++) {
                if (!(files[i] instanceof File)) {
                    files[i] = newFile;
                    break;
                }

                if(i + 1 == files.length) {
                    throw new NotEnoughSpaceException();
                }
            }


    }




        public void setLoggedIn(Boolean loggedIn) {
            this.loggedIn = loggedIn;
        }

        public Boolean getLoggedIn() {
            return loggedIn;
        }

    }
