package com.company;

import com.company.exceptions.NotEnoughPermissionsException;
import com.company.exceptions.NotEnoughSpaceException;

import java.nio.file.FileSystemNotFoundException;

public class Main {

    public static void main(String[] args) {

        FileManager fileManager = new FileManager();


        try {
             fileManager.createFile("teste1");
        } catch (NotEnoughPermissionsException ex) {
            System.out.println("Please log in first!");
        } catch (NotEnoughSpaceException ex) {
            System.out.println("Please delete a file first!");
        }

    }
}
