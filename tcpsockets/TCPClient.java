package src.org.academiadecodigo.tailormoons.tcpsockets;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

public class TCPClient {

    public static void main(String[] args) {


        String serverName = args[0];
        int portNumber = Integer.parseInt(args[1]);


        try {
            Socket clientSocket = new Socket(serverName, portNumber);

            PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);
            BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));


            Scanner scanner = new Scanner(System.in);
            //out.write(scanner.next());
            //out.write(scanner.nextLine());
            out.println(scanner.nextLine());

            //System.out.println(in.readLine());


        } catch (IOException e) {
            e.printStackTrace();
        }


     /*   // STEP1: Get the host and the port from the command-line
        String hostName = args[0];
        int portNumber = Integer.parseInt(args[1]);

// STEP2: Open a client socket, blocking while connecting to the server
        Socket clientSocket = new Socket(hostName, portNumber);

// STEP3: Setup input and output streams
        PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);
        BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

// STEP4: Read from/write to the stream
// STEP5: Close the streams
// STEP6: Close the sockets*/
    }
}
