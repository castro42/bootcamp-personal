package src.org.academiadecodigo.tailormoons.tcpsockets;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;


public class TCPServer {

    public static void main(String[] args) {

        int portNumber = 0; // 0 = port automatically allocated
        ServerSocket serverSocket;


        try {
            portNumber = Integer.parseInt(args[0]);

            //Creates a server socket, bound to the specified port
            serverSocket = new ServerSocket(portNumber);

            while(!serverSocket.isClosed()) {

                //Listens for a connection to be made to this socket and accepts it. The method blocks until a connection is made.
                Socket clientSocket = serverSocket.accept();

                PrintWriter out =  new PrintWriter(clientSocket.getOutputStream(), true);
                BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

                //out.println("Welcome to the chat server!");

                Scanner scanner = new Scanner(System.in);
                //out.write(scanner.next());
                //out.write(scanner.nextLine());
                //out.println(scanner.nextLine());

                System.out.println(in.readLine());
            }

            } catch (NumberFormatException | IOException e) {
                System.out.println(e.getMessage());
            }




// STEP4: Read from/write to the stream
// STEP5: Close the streams
// STEP6: Close the sockets

        /*// STEP3: Setup input and output streams
        PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);
        BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));*/






    }
}
