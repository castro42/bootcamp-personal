package org.academiadecodigo.stackinvaders.hotelcalifornia;

public class Main {

    public static void main(String[] args) {

        Room room1 = new Room(1);
        Room room2 = new Room(2);
        Room room3 = new Room(3);
        room1.toString();
        room2.toString();
        room3.toString();
        room1.occupy();
        room2.occupy();

        Room[] roomsArray = {room1, room2, room3};
        Hotel hotel1 = new Hotel("Ritz", roomsArray);

        Guest guest1 = new Guest("Pedro");
        guest1.getName();
        guest1.checkIn(hotel1);
        guest1.forgotRoomNumber();
        guest1.checkOut();
        guest1.forgotRoomNumber();





    }
}
