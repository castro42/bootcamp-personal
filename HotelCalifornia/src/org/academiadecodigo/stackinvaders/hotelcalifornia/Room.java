package org.academiadecodigo.stackinvaders.hotelcalifornia;

public class Room {

    private boolean isAvailable = true;
    private int roomNumber;
    public static Room NOT_FOUND = new Room(-1);

    public Room(int roomNumber) {
        this.roomNumber = roomNumber;
    }

    public boolean isAvailable() {
        return isAvailable;
    }

    public void occupy() {
        isAvailable = false;
    }

    public void free() {
        isAvailable = true;
    }

    public int getRoomNumber() {
        return roomNumber;
    }

    public void setRoomNumber(int number) {
        this.roomNumber = number;
    }
}
