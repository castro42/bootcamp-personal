package org.academiadecodigo.stackinvaders.hotelcalifornia;

public class Hotel {
    String name;
    private Room[] rooms;

    public Hotel(String name, Room[] rooms) {
        this.name = name;
        this.rooms = rooms;
    }

    public void checkIn(Guest guest) {
        if (isThereAFreeRoom() == false) {
            System.out.println("We're sorry but the hotel is full!");
            return;
        }
        Room freeRoom = getFreeRoom();
        guest.saveRoom(freeRoom);
        System.out.println("Welcome to our hotel!");
    }

    public void checkOut(Room room) {

        room.free();
        System.out.println("Thank you for your stay!");
        room.setRoomNumber(-1);
    }

    public boolean isThereAFreeRoom() {

        for (Room room : rooms) {
            if (room.isAvailable()) {
                return true;
            }
        }

    return false;

    }


    public Room getFreeRoom() {
        for (Room room : rooms) {
            if (room.isAvailable()) {
                room.occupy();
                return room;
            }
        }
        return Room.NOT_FOUND;
    }



}