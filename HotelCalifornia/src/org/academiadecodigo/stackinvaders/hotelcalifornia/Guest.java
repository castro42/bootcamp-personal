package org.academiadecodigo.stackinvaders.hotelcalifornia;

public class Guest {

    private String name;
    private Hotel hotel;
    private Room room;


    public Guest(String name) {
        this.name = name;
    }

    public void checkIn(Hotel hotel) {
        this.hotel = hotel;
        System.out.println("teste " + this.getName());
        this.hotel.checkIn(this);
        //Room room = hotel.getFreeRoom();

    }

    public void checkOut() {
        hotel.checkOut(room);
    }

    public String getName() {
        System.out.println(name);
        return name;
    }

    public void saveRoom(Room room) {
        this.room = room;
    }

    //bad design? Guest should ask the Hotel not the Room, right?!
    public void forgotRoomNumber() {
        System.out.println("Your room is room number: " + room.getRoomNumber());
    }



}
