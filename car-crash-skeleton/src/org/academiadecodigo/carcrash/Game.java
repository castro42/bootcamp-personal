package org.academiadecodigo.carcrash;

import org.academiadecodigo.carcrash.cars.Car;
import org.academiadecodigo.carcrash.cars.CarFactory;
import org.academiadecodigo.carcrash.field.Field;
import org.academiadecodigo.carcrash.field.Position;

import java.sql.SQLOutput;

public class Game {

    public static final int MANUFACTURED_CARS = 20;

    /** Container of Cars */
    private Car[] cars;

    /** Animation delay */
    private int delay;

    public Game(int cols, int rows, int delay) {

        Field.init(cols, rows);
        this.delay = delay;

    }

    /**
     * Creates a bunch of cars and randomly puts them in the field
     */
    public void init() {

        cars = new Car[MANUFACTURED_CARS];
        for (int i = 0; i < cars.length; i++) {
            cars[i] = CarFactory.getNewCar();
        }

        Field.draw(cars);

    }

    /**
     * Starts the animation
     *
     * @throws InterruptedException
     */
    public void start() throws InterruptedException {

        while (true) {

            // Pause for a while
            Thread.sleep(delay);

            // Move all cars
            moveAllCars();

            // Update screen
            Field.draw(cars);

        }

    }

    private void moveAllCars() {
        for (int i=0; i < cars.length; i++) {
            Position pos = cars[i].getPos();
            int col = pos.getCol();
            int row = pos.getRow();

            int maxCols = Field.getWidth();
            int maxRow = Field.getHeight();

            //MOVE COL
            //random number 0 or 1
            int randomColBit = ((int) (Math.random() * 2));
            System.out.println("Random col " + randomColBit);
            System.out.println("current column " + col);

            //move col +1 if there's space
            if((randomColBit == 1) && (col < 100)) {
                col = col + 1;
            } else if ((randomColBit == 1) && (col >= 100)) {
                col = col -1;
            }

            //move col -1 if there's space
            if((randomColBit == 0) && (col > 0)) {
                col = col - 1;
            } else if ((randomColBit == 0) && (col <= 0)) {
                col = col + 1;
            }
            System.out.println("new column " + col);

            //MOVE ROW
            //random number 0 or 1
            int randomRowBit = ((int) (Math.random() * 2));
            System.out.println("Random row " + randomRowBit);
            System.out.println("current row " + row);

            //move row +1 if there's space
            if((randomRowBit == 1) && (row < 25)) {
                row = row + 1;
            } else if ((randomRowBit == 1) && (row >= 25)) {
                row = row -1;
            }

            //move row -1 if there's space
            if((randomRowBit == 0) && (row > 0)) {
                row = row - 1;
            } else if ((randomRowBit == 0) && (row <= 0)) {
                row = row + 1;
            }
            System.out.println("new row " + row);

            pos.setCol(col);
            pos.setRow(row);
        }
    }

}
