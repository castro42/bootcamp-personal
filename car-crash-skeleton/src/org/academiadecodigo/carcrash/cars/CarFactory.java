package org.academiadecodigo.carcrash.cars;

import org.academiadecodigo.carcrash.field.Field;
import org.academiadecodigo.carcrash.field.Position;

public class CarFactory {

    public static  Car getNewCar() {

        //TODO: pq consigo aceder a estes metodos se classe não é static?
        int height = Field.getHeight();
        int width = Field.getWidth();
        Position pos = randomPositionObject(width, height);

        int randomNumber = (int) (Math.random() * 2);

        if(randomNumber == 0) {
            return new Fiat(pos);
        } else {
            return new Mustang(pos);
        }


    }

    //TODO: PROBLEM: we can instantiate different cars with the same position
    public static Position randomPositionObject(int maxCols, int maxRows) {
        int col = (int) (Math.random() * (maxCols + 1));
        int row = (int) (Math.random() * (maxRows + 1));
        return new Position(col, row);
    }

}
