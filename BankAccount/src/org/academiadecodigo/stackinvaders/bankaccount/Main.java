package org.academiadecodigo.stackinvaders.bankaccount;

public class Main {

    public static void main(String[] args) {
        Person p1 = new Person("Pedro");
        Person p2 = new Person("Sid");


        p1.storeInWallet(600);
        p1.printMoneyInWallet();
        p1.moveFromWalletToAccount(300);
        p1.printMoneyInAccount();

        p2.storeInWallet(1000);
        p2.printMoneyInWallet();
        p2.moveFromWalletToAccount(500);
        p2.printMoneyInAccount();



        p1.moveFromBankToBank(p2, 5000);
        p1.printMoneyInAccount();
        p2.printMoneyInAccount();







    }
}
