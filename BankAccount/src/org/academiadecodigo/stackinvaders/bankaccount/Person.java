package org.academiadecodigo.stackinvaders.bankaccount;

public class Person {
    private String name;
    private Account myAccount;
    private Wallet myWallet;

    public Person(String name) {
        this.name = name;
        myWallet = new Wallet();
        myAccount = new Account();
    }

    public void storeInWallet(int amount) {
        myWallet.store(amount);
    }

    public void spend(int amount) {
        myWallet.take(amount);
    }

    public void printMoneyInWallet() {
        System.out.println("You have " + myWallet.getBalance() + " in your wallet.");
    }

    public void printMoneyInAccount() {
        System.out.println("You have " + myAccount.getBalance() + " in your account.");
    }

    public void moveFromWalletToAccount(int amount) {
        myWallet.take(amount);
        myAccount.addToBalance(amount);
    }

    public void moveFromBankToBank(Person friend, int amount) {
        myAccount.wireTransfer(friend, amount);
    }

    //removes from account and stores in wallet
    public void takeFromAccount(int amount) {
        myAccount.withdrawMoney(amount);
        storeInWallet(amount);
    }


}