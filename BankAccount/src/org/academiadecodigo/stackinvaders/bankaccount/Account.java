package org.academiadecodigo.stackinvaders.bankaccount;

public class Account {

    private int balance;

    public void addToBalance(int amount) {
        balance = balance + amount;
    }

    public void withdrawMoney(int amount) {
        balance = balance - amount;
    }

    public void wireTransfer(Person friend, int amount) {
        this.withdrawMoney(amount);
        friend.storeInWallet(amount);
        friend.moveFromWalletToAccount(amount);
    }

    public int getBalance()  {
        return balance;
    }


}
