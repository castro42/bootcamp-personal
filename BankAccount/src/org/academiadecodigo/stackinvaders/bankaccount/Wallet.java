package org.academiadecodigo.stackinvaders.bankaccount;

public class Wallet {

    private int balance;

    //adds money to the Wallet
    public void store(int amount) {
        this.balance = balance + amount;
    }

    //sex, drugs and rock&roll
    public void take(int amount) {
        if(amount <= balance) {
            balance = balance - amount;
        } else {
            System.out.println("You don't have enough funds!");
        }

    }

    public int getBalance() {
        return balance;
    }
}
