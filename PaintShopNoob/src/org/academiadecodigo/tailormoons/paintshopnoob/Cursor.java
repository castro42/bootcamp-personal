package org.academiadecodigo.tailormoons.paintshopnoob;

import org.academiadecodigo.simplegraphics.graphics.Movable;
import org.academiadecodigo.simplegraphics.keyboard.Keyboard;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEvent;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEventType;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardHandler;
import org.academiadecodigo.simplegraphics.test.KeyboardListener;
import org.academiadecodigo.tailormoons.paintshopnoob.gfx.SimpleGfxGridPosition;
import org.academiadecodigo.tailormoons.paintshopnoob.grid.GridColor;

public class Cursor implements Movable {

    private SimpleGfxGridPosition cursorPosition;
    private Keyboard keyboard;
    private KeyboardEvent keyboardEvent;
    private CursorKeyboardListener cursorKeyboardListener;

    public Cursor(SimpleGfxGridPosition cursorPosition) {
        this.cursorPosition = cursorPosition;
        cursorPosition.setColor(GridColor.GREEN);
        cursorKeyboardListener = new CursorKeyboardListener(cursorPosition.rectangle, this);
        Keyboard keyboard = new Keyboard(cursorKeyboardListener);
        cursorPosition.rectangle.fill();

        //TODO: put this in a method somewhere?
        KeyboardEvent right = new KeyboardEvent();
        right.setKey(KeyboardEvent.KEY_RIGHT);
        right.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        keyboard.addEventListener(right);

        KeyboardEvent left = new KeyboardEvent();
        left.setKey(KeyboardEvent.KEY_LEFT);
        left.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        keyboard.addEventListener(left);

        KeyboardEvent up = new KeyboardEvent();
        up.setKey(KeyboardEvent.KEY_UP);
        up.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        keyboard.addEventListener(up);

        KeyboardEvent down = new KeyboardEvent();
        down.setKey(KeyboardEvent.KEY_DOWN);
        down.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        keyboard.addEventListener(down);

        KeyboardEvent space = new KeyboardEvent();
        space.setKey(KeyboardEvent.KEY_SPACE);
        space.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        keyboard.addEventListener(space);


    }

    @Override
    public void translate(double v, double v1) {

    }

    private void setUpKeyboardEvents() {

    }

    public SimpleGfxGridPosition getCursorPosition() {
        return cursorPosition;
    }

}
