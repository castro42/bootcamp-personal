package org.academiadecodigo.tailormoons.paintshopnoob.gfx;

import org.academiadecodigo.simplegraphics.graphics.Rectangle;
import org.academiadecodigo.tailormoons.paintshopnoob.grid.GridColor;
import org.academiadecodigo.tailormoons.paintshopnoob.grid.position.AbstractGridPosition;

public class SimpleGfxGridPosition extends AbstractGridPosition {

    //TODO: made it public so that class Cursor can access it without creating a setter.
    //TODO: bad design. find a better way!
    public Rectangle rectangle;

    public SimpleGfxGridPosition(int col, int row, SimpleGfxGrid grid) {
        super(col, row, grid);
        rectangle = new Rectangle(grid.columnToX(col), grid.rowToY(row), SimpleGfxGrid.CELL_SIZE, SimpleGfxGrid.CELL_SIZE);
        //rectangle.fill();
        rectangle.draw();
    }

    @Override
    public void setColor(GridColor color) {
        rectangle.setColor(SimpleGfxColorMapper.getColor(color));
        super.setColor(color);
    }

    @Override
    public void show() {
        rectangle.draw();
    }

    @Override
    public void hide() {
        rectangle.delete();
    }
}
