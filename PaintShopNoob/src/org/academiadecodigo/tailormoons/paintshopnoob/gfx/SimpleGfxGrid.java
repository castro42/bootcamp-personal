package org.academiadecodigo.tailormoons.paintshopnoob.gfx;

import org.academiadecodigo.simplegraphics.graphics.Rectangle;
import org.academiadecodigo.tailormoons.paintshopnoob.Cursor;
import org.academiadecodigo.tailormoons.paintshopnoob.grid.Grid;
import org.academiadecodigo.tailormoons.paintshopnoob.grid.position.GridPosition;


public class SimpleGfxGrid implements Grid {

    public static final int PADDING = 10;
    public static final int CELL_SIZE = 20;

    private int rows;
    private int cols;
    private int cellSize;
    private int x;
    private int y;
    private int width;
    private int height;
    private Rectangle rectangle;
    private static SimpleGfxGridPosition[][] gridPositions; //TODO: static?

    public SimpleGfxGrid(int cols, int rows) {
        rectangle = new Rectangle(PADDING, PADDING, cols*CELL_SIZE, rows*CELL_SIZE);
        this.rows = rows;
        this.cols = cols;
        gridPositions = new SimpleGfxGridPosition[rows][cols];

    }

    @Override
    public void init() {
        rectangle.draw();
        createAllGridPositions();
        createCursor();
    }

    @Override
    public int getCols() {
        return cols;
    }

    @Override
    public int getRows() {
        return rows;
    }

    @Override
    public GridPosition makeGridPosition(int row, int col) {
        return new SimpleGfxGridPosition(row, col, this);
    }

    @Override
    public void createAllGridPositions() {
        //rows
        for (int i = 0; i < gridPositions.length ; i++) {
            //cols
            for (int j = 0; j < gridPositions[i].length ; j++) {
                System.out.println("[" + i + "]" + " " + "[" + j + "]" );
                gridPositions[i][j] = (SimpleGfxGridPosition) makeGridPosition(i, j);
            }
        }

    }

    @Override
    public void createCursor() {
        int cursorCol = getCols()/2;
        int cursorRow = getRows()/2;
        Cursor cursor = new Cursor(new SimpleGfxGridPosition(cursorCol, cursorRow, this));
        //gridPositions[cursorRow][cursorCol].hide();
    }

    public void printGridPositions() {
        // Loop through all rows
        for (int i = 0; i < gridPositions.length; i++) {
            // Loop through all elements of current row
            for (int j = 0; j < gridPositions[i].length; j++) {
                System.out.print(gridPositions[i][j] + " ");
            }

        }
    }

    /**
     * Auxiliary method to compute the y value that corresponds to a specific row
     * @param row index
     * @return y pixel value
     */
    public int rowToY(int row) {
        return PADDING + CELL_SIZE * row;
    }

    /**
     * Auxiliary method to compute the x value that corresponds to a specific column
     * @param column index
     * @return x pixel value
     */
    public int columnToX(int column) {
        return PADDING + CELL_SIZE * column;
    }

    public static SimpleGfxGridPosition[][] getGridPositions() {
        return gridPositions;
    }

    /*public void changeGridPosition(row, col) {
        gridPositions[row][col]
    }*/
}
