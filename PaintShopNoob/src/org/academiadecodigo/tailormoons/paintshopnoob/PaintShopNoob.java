package org.academiadecodigo.tailormoons.paintshopnoob;

import org.academiadecodigo.tailormoons.paintshopnoob.grid.Grid;
import org.academiadecodigo.tailormoons.paintshopnoob.grid.GridFactory;
import org.academiadecodigo.tailormoons.paintshopnoob.grid.GridType;

public class PaintShopNoob {

    private Grid grid;

    public PaintShopNoob(GridType gridType, int cols, int rows) {
        grid = GridFactory.makeGrid(gridType, cols, rows);
    }

    public void init() {
        grid.init();
        grid.makeGridPosition(0,0);
        grid.createAllGridPositions();

        grid.printGridPositions();



    }
}
