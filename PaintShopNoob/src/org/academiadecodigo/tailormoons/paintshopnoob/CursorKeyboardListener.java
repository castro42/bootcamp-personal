package org.academiadecodigo.tailormoons.paintshopnoob;

import org.academiadecodigo.simplegraphics.graphics.Rectangle;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEvent;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardHandler;

public class CursorKeyboardListener implements KeyboardHandler {

    private Rectangle rectangle;
    private Cursor cursor;

    public CursorKeyboardListener(Rectangle rectangle, Cursor cursor) {
        this.rectangle = rectangle;
        this.cursor = cursor;
    }

    //TODO: REMOVE HARDCODED MOVEMENTS
    @Override
    public void keyPressed(KeyboardEvent event) {
        switch (event.getKey()) {
            case KeyboardEvent.KEY_UP:
                rectangle.translate(0, -20);
                int col = cursor.getCursorPosition().getCol();
                int row = cursor.getCursorPosition().getRow();
                cursor.getCursorPosition().setPosNoShow(col, row-1);
                System.out.println(cursor.getCursorPosition().getRow());
                System.out.println(cursor.getCursorPosition().getCol());
                break;
            case KeyboardEvent.KEY_DOWN:
                rectangle.translate(0, 20);
                col = cursor.getCursorPosition().getCol();
                row = cursor.getCursorPosition().getRow();
                cursor.getCursorPosition().setPosNoShow(col, row+1);
                break;
            case KeyboardEvent.KEY_LEFT:
                rectangle.translate(-20, 0);
                col = cursor.getCursorPosition().getCol();
                row = cursor.getCursorPosition().getRow();
                cursor.getCursorPosition().setPosNoShow(col-1, row);
                break;
            case KeyboardEvent.KEY_RIGHT:
                rectangle.translate(20, 0);
                col = cursor.getCursorPosition().getCol();
                row = cursor.getCursorPosition().getRow();
                cursor.getCursorPosition().setPosNoShow(col+1, row);
                break;
            case KeyboardEvent.KEY_SPACE:
                col = cursor.getCursorPosition().getCol();
                row = cursor.getCursorPosition().getRow();
                //SimpleGfxGrid.getGridPositions();
                //rectangle.fill();
                break;

        }
    }

    @Override
    public void keyReleased(KeyboardEvent event) {

    }

}
