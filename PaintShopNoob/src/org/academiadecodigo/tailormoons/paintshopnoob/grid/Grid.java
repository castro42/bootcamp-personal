package org.academiadecodigo.tailormoons.paintshopnoob.grid;

import org.academiadecodigo.tailormoons.paintshopnoob.grid.position.GridPosition;

public interface Grid<printGridPositions> {

    /**
     * Initializes the grid
     */
    public void init();

    public int getCols();

    public int getRows();

    public GridPosition makeGridPosition(int row, int col);

    public void createAllGridPositions();

    public void createCursor();

    public void printGridPositions();
}
