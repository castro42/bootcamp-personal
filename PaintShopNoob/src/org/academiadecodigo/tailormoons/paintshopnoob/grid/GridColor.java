package org.academiadecodigo.tailormoons.paintshopnoob.grid;

/**
 * The available grid colors
 */
public enum GridColor {

    GREEN,
    BLACK,
    WHITE,
    RED,
    BLUE,
    MAGENTA,
    NOCOLOR

}
